# CarCar

Team:

* Sarah Ahn - Service
* Jonathan Roman - Sales

## Design

We used Django for the back-end and React and Bootstrap for the front-end.

## Service microservice

In the service microservice models, I used AutomobileVO, Technician, and Appointment.
The AutomobileVO is a value object used in both the service microservice and the sales microservice.

## Sales microservice

In the models, I have an automobileVO. This has properties of VIN. The customer model has properties of name, address, and phone number. The salesperson model has properties of name and employee number. The sales record model has properties of sale price and three foreign keys, salesperson, customer, and vin.
In the views, I have four encoders. AutomobileVOEncoder, CustomerEncoder, SalesPersonEncoder, and SalesRecordEncoder. The properties for these encoders match the models. I used encoders so that I may pass data directly with the encoder. Encoders also allow for more organized code. I put safe=False, when it was not serializable. The encoders were placed in an encoders.py file for better code organization.
In poller, the poll time was changed from 60 to 20.
Classes were used in the sales react forms and lists. The properties match the properties of the models. For handlesubmit, I used preventDefault so that it does not restart the page each time.

In Inventory, for the Automobile form and list in react, I used functions.
The automobile form allows one to input VIN, color, year, and select one of the models from a dropdown option. When adding, the page will rerender.
The automobile list shows the year, manufacturer, model, color, and VIN. A delete option was also included with a forceUpdate to rerender the page after a deletion.
The model form will handle name change, picture change, and  manufacturer change. The name and photo are text boxes whereas the manufacturer must be chosen from one of the manufacturers in the dropdown option.
The model list will display the manufacturer, model, and photo that was submitted. A deletion option was also used in this list.
These inventory react files were integrated into the Nav under the Inventory dropdown. They were imported and routed in the App.js.
