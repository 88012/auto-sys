# Generated by Django 4.0.3 on 2022-10-28 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_rename_employee_name_appointment_technician'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='technician',
        ),
        migrations.AddField(
            model_name='appointment',
            name='technician_id',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
