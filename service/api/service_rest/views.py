from django.shortcuts import render
from .models import Appointment, Technician, AutomobileVO
from .encoders import TechniciansListEncoder, AppointmentsListEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods



@require_http_methods(["GET"])
def api_list_techs(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechniciansListEncoder,
        )


@require_http_methods(["POST"])
def api_create_tech(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechniciansListEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_show_tech(request, pk):
    if request.method == "GET":
        try:
            technicians = Technician.objects.get(id=pk)
            return JsonResponse(
                technicians,
                encoder = TechniciansListEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status = 400,
            )


@require_http_methods(["DELETE"])
def api_delete_tech(request, pk):
    if request.method == "DELETE":
        try:
            technicians = Technician.objects.get(id=pk)
            technicians.delete()
            return JsonResponse(
                technicians,
                encoder = TechniciansListEncoder,
                safe = False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status = 400,
            )


@require_http_methods(["GET"])
def api_list_appts(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentsListEncoder,
        )


@require_http_methods(["POST"])
def api_create_appt(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            vehicle = AutomobileVO.objects.get(vin=content["vin"])
            print(vehicle)
            content["vin"] = vehicle
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentsListEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentsListEncoder,
                safe=False,
            )


@require_http_methods(["GET"])
def api_show_appt(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointments,
                encoder = AppointmentsListEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment"},
                status = 400,
            )


@require_http_methods(["PUT"])
def api_update_appt(request, pk):
    if request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointments = Appointment.objects.get(id=pk)

            props = ["vin", "customer_name", "dealer_sold", "date", "time", "technician", "reason", "finished"]
            for prop in props:
                if prop in content:
                    setattr(appointments, prop, content[prop])
            appointments.save()
            return JsonResponse(
                appointments,
                encoder = AppointmentsListEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment"},
                status = 400
            )


@require_http_methods(["DELETE"])
def api_delete_appt(request, pk):
    if request.method == "DELETE":
        try:
            appointments = Appointment.objects.get(id=pk)
            appointments.delete()
            return JsonResponse(
                appointments,
                encoder = AppointmentsListEncoder,
                safe = False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment"},
                status = 400,
            )


@require_http_methods(["GET"])
def api_service_history(request, vin):
    appointments = Appointment.objects.filter(vin=vin)
    return JsonResponse(
        appointments,
        encoder=AppointmentsListEncoder,
        safe=False,
    )
