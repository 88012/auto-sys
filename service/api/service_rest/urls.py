from django.urls import path
from .views import (
    api_list_techs,
    api_create_tech,
    api_show_tech,
    api_delete_tech,
    api_list_appts,
    api_create_appt,
    api_show_appt,
    api_service_history,
)


urlpatterns = [
    path("techs/", api_list_techs, name="api_list_techs"),
    path("techs/new/", api_create_tech, name="api_create_tech"),
    path("techs/<int:pk>/", api_show_tech, name="api_show_tech"),
    path("techs/<int:pk>/delete/", api_delete_tech, name="api_delete_tech"),
    path("service/", api_list_appts, name="api_list_appts"),
    path("service/new/", api_create_appt, name="api_create_appt"),
    path("service/<int:pk>/", api_show_appt, name="api_show_appt"),
    path("service/history/<str:vin>/", api_service_history, name="api_service_history"),
]
