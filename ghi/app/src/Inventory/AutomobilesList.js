import React, {useState, useEffect} from 'react';

function AutomobilesList() {
    const [automobiles, setAutomobiles] = useState([]);
    useEffect(() => {
        const getAutomobileData = async () => {
            const automobileResponse = await fetch("http://localhost:8100/api/automobiles/");
            const automobileData = await automobileResponse.json();

            setAutomobiles(automobileData.autos);
        };
        getAutomobileData();
    }, []);

    async function handleDelete(vin) {
        const automobilesUrl = `http://localhost:8100/api/automobiles/${vin}`;
       const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const automobileResponse = await fetch(automobilesUrl, fetchConfig);
        if (automobileResponse.ok) {
            setAutomobiles(automobiles.filter(automobile => automobile.vin !== vin))
        }
        else {
            throw new Error ("Error");
        }
        this.forceUpdate()
    };

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Automobiles</h1>
              <table className="table table-striped">
                  <thead>
                  <tr>
                      <th>Year</th>
                      <th>Manufacturer</th>
                      <th>Model</th>
                      <th>Color</th>
                      <th>VIN</th>
                  </tr>
                  </thead>
                  <tbody>
                      {automobiles.map((model)=> {
                          return(
                              <tr key={model.id}>
                                  <td>{model.year}</td>
                                  <td>{model.model.manufacturer.name}</td>
                                  <td>{model.model.name}</td>
                                  <td>{model.color}</td>
                                  <td>{model.vin}</td>
                                  <td><button className="btn btn-danger" onClick={() => handleDelete(model.vin)}>Delete</button></td>
                              </tr>
                          );
                      })}
                  </tbody>
              </table>
              </div>
          </div>
      </div>
      );
                  }

export default AutomobilesList;
