import React, {useState, useEffect} from 'react';

function ModelsList() {
    const [models, setModels] = useState([]);
    useEffect(() => {
        const getModelData = async () => {
            const modelResponse = await fetch("http://localhost:8100/api/models/");
            const modelData = await modelResponse.json();

            setModels(modelData.models);
        };
        getModelData();
    }, []);

    async function handleDelete(id) {
        const modelsUrl = `http://localhost:8100/api/models/${id}`;
       const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const modelResponse = await fetch(modelsUrl, fetchConfig);
        if (modelResponse.ok) {
            setModels(models.filter(model => model.id !== id))
        }
        else {
            throw new Error ("Error");
        }
    };

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Models</h1>
              <table className="table table-striped">
                  <thead>
                  <tr>
                      <th>Manufacturer</th>
                      <th>Model</th>
                      <th>Photo</th>
                  </tr>
                  </thead>
                  <tbody>
                      {models.map(model=> {
                          return(
                              <tr key={model.id}>
                                  <td>{model.manufacturer.name}</td>
                                  <td>{model.name}</td>
                                  <td><img src={model.picture_url} alt="model" width= "130" height="95"/></td>
                                  <td><button className="btn btn-danger" onClick={() => handleDelete(model.id)}>Delete</button></td>
                              </tr>
                          );
                      })}
                  </tbody>
              </table>
              </div>
          </div>
      </div>
      );
                  }

export default ModelsList;
