import React from "react";


class ManufacturersList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8100/api/manufacturers/');
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers:data.manufacturers});
        }
    }

    async handleDelete(id) {
        const manufacturersUrl = `http://localhost:8100/api/manufacturers/${id}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const manufacturerResponse = await fetch(manufacturersUrl, fetchConfig);
        if (manufacturerResponse.ok) {
            this.setState ({manufacturers: this.state.manufacturers.filter(manufacturer => manufacturer.id != id)})
        }
        else {
            throw new Error ("Error");
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                                <td><button className="btn btn-danger" onClick={() => this.handleDelete(manufacturer.id)}>Delete</button></td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
    };
}

export default ManufacturersList;
