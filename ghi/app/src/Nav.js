import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
            <a className="nav-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" aria-haspopup="true">Sales</a>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/customers/new">Add a Potential Customer</NavLink></li>
              <li><NavLink className="dropdown-item" to="/salespeople/new">Add a Salesperson</NavLink></li>
              <li><NavLink className="dropdown-item" to="/salesrecords/new">Add a Sales Record</NavLink></li>
              <li><NavLink className="dropdown-item" to="/sales/list">Sales List</NavLink></li>
              <li><NavLink className="dropdown-item" to="/employeesales/list">Salesperson's Sales List</NavLink></li>
            </ul>
            <a className="nav-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" aria-haspopup="true">Service</a>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/techs/new">Add a New Technician</NavLink></li>
              <li><NavLink className="dropdown-item" to="/techs/list">Technicians List</NavLink></li>
              <li><NavLink className="dropdown-item" to="/service/new">Add an Appointment</NavLink></li>
              <li><NavLink className="dropdown-item" to="/service/list">Appointments List</NavLink></li>
              <li><NavLink className="dropdown-item" to="/service/history">Service History List</NavLink></li>
            </ul>
            <a className="nav-item dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" aria-haspopup="true">Inventory</a>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/automobiles/new">Add a New Automobile</NavLink></li>
              <li><NavLink className="dropdown-item" to="/automobiles/list">Automobiles List</NavLink></li>
              <li><NavLink className="dropdown-item" to="/manufacturers/new">Add a New Manufacturer</NavLink></li>
              <li><NavLink className="dropdown-item" to="/manufacturers/list">Manufacturers List</NavLink></li>
              <li><NavLink className="dropdown-item" to="/models/new">Add a New Vehicle Model</NavLink></li>
              <li><NavLink className="dropdown-item" to="/models/list">Vehicle Models List</NavLink></li>
            </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
