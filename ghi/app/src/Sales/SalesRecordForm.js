import React from 'react';

class SalesRecordForm extends React.Component {
    state = {
        vin:'',
        vins:[],
        sales_person: '',
        salespersons:[],
        customer:'',
        customers:[],
        sale_price:'',
        };

    async componentDidMount() {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const salespersonsUrl = 'http://localhost:8090/api/salespersons/'
        const customerUrl = 'http://localhost:8090/api/customers/'
        const salesUrl = 'http://localhost:8090/api/sales/'

        const automobilesresponse = await fetch(automobilesUrl);
        const salespersonsresponse = await fetch(salespersonsUrl);
        const customerresponse = await fetch(customerUrl);
        const salesresponse = await fetch(salesUrl);

        if (automobilesresponse.ok && salespersonsresponse.ok && customerresponse.ok && salesresponse.ok) {
            const automobilesdata = await automobilesresponse.json();
            const salespersonsdata = await salespersonsresponse.json();
            const customerdata = await customerresponse.json();
            const salesdata = await salesresponse.json();
            const soldCars = salesdata.sales.map(sale => sale.vin);
            const unsoldCars = automobilesdata.autos.filter(autos => !soldCars.includes(autos.vin))
            this.setState({ vins:unsoldCars, salespersons: salespersonsdata.salespersons, customers: customerdata.customers, });
        }
      }

    handleChange = async (event) => {
        const value = event.target.value
        const key = event.target.name
        const newState = {}
        newState[key] = value
        this.setState(newState)
      }

    handleSubmit = async (event) => {
        event.preventDefault();
        let data = {...this.state};
        delete data.vins;
        delete data.salespersons;
        delete data.customers;
        const salesrecordUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(salesrecordUrl, fetchConfig);
        if (response.ok) {
            let newsalesrecord = await response.json();
            this.setState({
                vin: '',
                sales_person: '',
                customer: '',
                sale_price: '',
            });
        }
      }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new sales record</h1>
                    <form onSubmit={this.handleSubmit} id="create-salesrecord-form">
                    <div className="form-floating mb-3">
                        <select onChange={this.handleChange} value={this.state.vin} required name="vin" id="vin" className="form-select" >
                        <option value="">Automobile</option>
                        {this.state.vins.map(vin => {return (
                            <option key={vin.vin} value={vin.vin}>{vin.model.name}</option>
                        )})}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={this.handleChange} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select" >
                        <option value="">Salesperson</option>
                        {this.state.salespersons.map(sales_person => {return (
                            <option key={sales_person.id} value={sales_person.name}>{sales_person.name}</option>
                        )})}
                        </select>
                    </div>
                    <div>
                        <select onChange={this.handleChange} value={this.state.customer} required name="customer" id="customer" className="form-select" >
                        <option value="">Customer</option>
                        {this.state.customers.map(customer => {return (
                            <option key={customer.id} value={customer.name}>{customer.name}</option>
                        )})}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} value={this.state.sale_price} placeholder="Sale price" name="sale_price" id="sale_price" required type="number" className="form-control" />
                        <label htmlFor="sale_price">Sale price</label>
                    </div>
                    <button className="btn btn-primary">Add</button>
                    </form>
                </div>
                </div>
            </div>
            );
        }
    }

export default SalesRecordForm;
