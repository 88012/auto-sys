import React from 'react';

class SalesPersonSalesList extends React.Component {
    state = {
        sales:[],
        salespersons:[],
        sales_person: '',
        filtSales:[],
        };

    async componentDidMount() {
        const salespersonsUrl = 'http://localhost:8090/api/salespersons/'
        const salesUrl = 'http://localhost:8090/api/sales/'

        const salespersonsresponse = await fetch(salespersonsUrl);
        const salesresponse = await fetch(salesUrl);

        if (salespersonsresponse.ok && salesresponse.ok) {
            const salespersonsdata = await salespersonsresponse.json();
            const salesdata = await salesresponse.json();
            this.setState({salespersons: salespersonsdata.salespersons, sales: salesdata.sales, });
        }
      }
      updateRecords = (sales_person) => {
        let newState = []
        this.state.sales.forEach((record => {
            if (record.sales_person.employee_number === Number(sales_person)) {
                newState.push(record)
            }
        }))
        this.setState({
            ...this.state,
            filtSales: newState
        })
      }

      handleChange = (event) => {
        const value = event.target.value;
        this.setState({
            sales_person:value,
        })
        this.updateRecords(value)
      }

      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Salesperson's Sales List</h1>
                <select onChange={this.handleChange} value={this.state.sales.sales_person} required name="sales_person" id="sales_person" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {this.state.salespersons.map(sales_person => {
                        return(
                            <option key={sales_person.employee_number} value={sales_person.employee_number}>{sales_person.name}</option>
                        )
                    })}
                </select>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer Name</th>
                        <th>VIN</th>
                        <th>Sale price $</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.filtSales.map((salesrecord, index)=> {
                            return(
                                <tr key={index}>
                                    <td>{salesrecord.sales_person.name}</td>
                                    <td>{salesrecord.customer.name}</td>
                                    <td>{salesrecord.vin.vin}</td>
                                    <td>{salesrecord.sale_price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        );
                    }
                }

export default SalesPersonSalesList;
