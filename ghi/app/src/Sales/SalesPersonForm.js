import React from 'react';

class SalesPersonForm extends React.Component {
    state = {
      name: '',
      employee_number:'',
      };

      handleNameChange = (event) => {
        const value = event.target.value;
        this.setState({ name: value });
      }

      handleEmployee_numberChange = (event) => {
        const value = event.target.value;
        this.setState({ employee_number: value });
      }

      handleSubmit = async (event) => {
        event.preventDefault();
        let data = {...this.state};

        const salespersonUrl = 'http://localhost:8090/api/salespersons/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
          this.setState({
            name: '',
            employee_number: '',
          });
        }
      }


      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a salesperson</h1>
                <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Salesperson's Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmployee_numberChange} value={this.state.employee_number} placeholder="Employee_number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                    <label htmlFor="employee_number">Salesperson's employee number</label>
                  </div>
                  <button className="btn btn-primary">Add</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

    export default SalesPersonForm;
