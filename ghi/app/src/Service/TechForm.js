import React from 'react';


class TechForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employee_name: '',
      employee_number: '',
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    let data = {...this.state};

    const technicianUrl = 'http://localhost:8080/api/techs/new/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        employee_name: '',
        employee_number: '',
      });
    }
  }

  handleChangeEmployeeName = (event) => {
    const value = event.target.value;
    this.setState({ employee_name: value });
  }

  handleChangeEmployeeNumber = (event) => {
    const value = event.target.value;
    this.setState({ employee_number: value });
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new technician</h1>
            <form onSubmit={this.handleSubmit} id="create-tech-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeEmployeeName} value={this.state.employee_name} placeholder="Employee Name" required type="text" name="employee_name" id="employee_name" className="form-control" />
                <label htmlFor="employee_name">Employee Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeEmployeeNumber} value={this.state.employee_number} placeholder="Employee Number" required type="number" name="employee_number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechForm;
