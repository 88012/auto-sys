import React from "react";


class ApptsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8080/api/service/');
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({appointments:data.appointments});
        }
    }

    async handleDelete(id) {
        const appointmentsUrl = `http://localhost:8080/api/service/${id}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const appointmentResponse = await fetch(appointmentsUrl, fetchConfig);
        if (appointmentResponse.ok) {
            this.setState ({appointments: this.state.appointments.filter(appointment => appointment.id != id)})
        }
        else {
            throw new Error ("Error");
        }
    }

    async handleFinished(id) {
        const appointmentsUrl = `http://localhost:8080/api/service/${id}`;
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const appointmentResponse = await fetch(appointmentsUrl, fetchConfig);
        if (appointmentResponse.ok) {
            this.setState ({appointments: this.state.appointments.filter(appointment => appointment.finished != true)})
        }
        else {
            throw new Error ("Error");
        }
    }

    render() {
        return (
            <div className="container">
                <h1>Service Appointments</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Dealer Sold</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Finished</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{JSON.stringify(appointment.dealer_sold)}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician_id}</td>
                                <td>{appointment.reason}</td>
                                <td>{JSON.stringify(appointment.finished)}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => this.handleDelete(appointment.id)}>Cancel</button>
                                    <button className="btn btn-success" onClick={() => this.handleFinished(appointment.id)}>Finished</button>
                                </td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
    };
}

export default ApptsList;
