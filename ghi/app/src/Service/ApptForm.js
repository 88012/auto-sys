import React from 'react';

class ApptForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: '',
            customer_name:'',
            dealer_sold:'',
            date: '',
            time: '',
            technician_id: '',
            technicians: [],
            reason: '',
            finished: '',
            };
    }

    componentDidMount = async () => {
        const url = 'http://localhost:8080/api/techs/';
        const response = await fetch(url);
        console.log(response);
        if (response.ok) {
          const data = await response.json();
          console.log(data);
          this.setState({ technicians: data.technicians });
        }
      }

      handleChangeVin = (event) => {
        const value = event.target.value;
        this.setState({ vin: value });
      }

      handleChangeCustomerName = (event) => {
        const value = event.target.value;
        this.setState({ customer_name: value });
      }

      handleChangeDealerSold = (event) => {
        const value = event.target.value;
        this.setState({ dealer_sold: value });
      }

      handleChangeDate = (event) => {
        const value = event.target.value;
        this.setState({ date: value });
      }

      handleChangeTime = (event) => {
        const value = event.target.value;
        this.setState({ time: value });
      }

      handleChangeTechnician = (event) => {
        const value = Number(event.target.value);
        this.setState({ technician_id: value });
      }

      handleChangeReason = (event) => {
        const value = event.target.value;
        this.setState({ reason: value });
      }

      handleChangeFinished = (event) => {
        const value = event.target.value;
        this.setState({ finished: value });
      }

      handleSubmit = async (event) => {
        event.preventDefault();
        let data = {...this.state};
        delete data.technicians;
        delete data.dealer_sold;
        delete data.finished;
        console.log(data)

        const appointmentsUrl = 'http://localhost:8080/api/service/new/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
          this.setState({
            vin: '',
            customer_name: '',
            dealer_sold: '',
            date:'',
            time:'',
            technician_id: '',
            reason: '',
            finished: '',
          });
        }
      }


      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new appointment</h1>
                <form onSubmit={this.handleSubmit} id="create-appt-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="VIN" type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>

                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeCustomerName} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="customer name" id="customer name" className="form-control" />
                    <label htmlFor="customer_name">Customer Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <h6>Dealer Sold</h6>
                  </div>
                  <div className="form-check">
                    <input onChange={this.handleChangeDealerSold} className="form-check-input" value={true} placeholder="Dealer Sold" type="radio" name="dealer sold" id="true" />
                    <label className="form-check-label mb-3" for="true">True</label>
                  </div>
                  <div className="form-check">
                    <input onChange={this.handleChangeDealerSold} className="form-check-input" value={false} placeholder="Dealer Sold" type="radio" name="dealer sold" id="false" />
                    <label className="form-check-label" for="false">False</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeDate} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                    <label htmlFor="date">Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeTime} value={this.state.time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                    <label htmlFor="time">Time</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeTechnician} value={this.state.technician_id} required name="Technician" id="technician">
                      <option value="">Choose a technician</option>
                      {this.state.technicians.map(technician => {
                        return (
                          <option key={technician.id} value={technician.id}>{technician.employee_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                    <label htmlFor="reason">Reason</label>
                  </div>
                  <div className="form-floating mb-3">
                    <h6>Finished</h6>
                  </div>
                  <div className="form-check">
                    <input onChange={this.handleChangeFinished} className="form-check-input" value={true} placeholder="Finished" type="radio" name="finished" id="true" />
                    <label className="form-check-label mb-3" for="true">True</label>
                  </div>
                  <div className="form-check">
                    <input onChange={this.handleChangeFinished} className="form-check-input" value={false} placeholder="Finished" type="radio" name="finished" id="false" />
                    <label className="form-check-label" for="false">False</label>
                  </div>
                  <button type="submit" className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

    export default ApptForm;
