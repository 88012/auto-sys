import React from "react";


class TechsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            technicians: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8080/api/techs/');
        const response = await fetch(url);
        if (response.ok) {
            const data =await response.json();
            this.setState({technicians:data.technicians});
        }
    }

    async handleDelete(id) {
        const techniciansUrl = `http://localhost:8080/api/techs/${id}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const technicianResponse = await fetch(techniciansUrl, fetchConfig);
        if (technicianResponse.ok) {
            this.setState ({technicians: this.state.technicians.filter(technician => technician.id != id)})
        }
        else {
            throw new Error ("Error");
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Employee Name</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.technicians.map(technician => {
                            return (
                                <tr key={technician.id}>
                                <td>{technician.employee_name}</td>
                                <td>{technician.employee_number}</td>
                                <td><button className="btn btn-danger" onClick={() => this.handleDelete(technician.id)}>Delete</button></td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
    };
}

export default TechsList;
