import requests
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import SalesRecord, SalesPerson, Customer, AutomobileVO
from .encoders import CustomerEncoder, SalesPersonEncoder, SalesRecordEncoder
import json

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Please try another employee number"}
            )

@require_http_methods(["DELETE", "GET"])
def api_delete_salesperson(request, pk):
    if request.method == "GET":
        try:
            salespersons = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                salespersons,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sorry, this does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sorry, this person does not exist"})

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Sorry, please try another customer instead"})


@require_http_methods(["DELETE", "GET"])
def api_delete_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Sorry, this does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Sorry, this customer does not exist"})

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)

        content = {
            **content,
            "sales_person": SalesPerson.objects.get(name=content["sales_person"]),
            "vin": AutomobileVO.objects.get(vin=content["vin"]),
            "customer": Customer.objects.get(name=content["customer"]),
        }
        content["vin"].sold = True
        content["vin"].save()
        requests.put(f'http://inventory-api:8000/api/automobiles/{content["vin"].vin}/', json={"sold": True})
        sales = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SalesRecordEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        try:
            sales = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sorry, this is not a valid sales record"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:

        try:
            salesperson_name = content["sales_person"]
            salesperson = SalesPerson.objects.get(name=salesperson_name)
            content["sales_person"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sorry, this salesperson does not exist"},
                status=400,
            )
        try:
            pot_customer = content["customer"]
            customer = Customer.objects.get(name=pot_customer)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Sorry, this customer does not exist"},
                status=400,
            )

        content = json.loads(request.body)
        SalesRecord.objects.filter(id=pk).update(**content)
        sale = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SalesRecordEncoder,
            safe=False,
        )
