from django.urls import path
from .api_views import api_list_sales, api_list_salesperson, api_list_customer, api_delete_customer, api_delete_salesperson


urlpatterns = [
    path("salespersons/", api_list_salesperson, name="list_salespersons"),
    path("salespersons/<int:pk>/", api_delete_salesperson, name="api_delete_salesperson"),
    path("customers/", api_list_customer, name="api_list_customer"),
    path("customers/<int:pk>/", api_delete_customer, name="api_delete_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
]
